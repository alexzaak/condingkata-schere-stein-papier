import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { GestureService } from './services/gesture.service';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [GestureService],
  bootstrap: [AppComponent]
})
export class AppModule { }
