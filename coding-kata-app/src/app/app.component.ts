import { Component } from '@angular/core';
import { GestureService } from './services/gesture.service';
import { Gesture } from './model/gesture.enum';
import { Result } from './model/result.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public gestureTypes = Gesture;
  public resultTypes = Result;

  public computer: Gesture;
  public player: Gesture;
  public result: Result;

  public scoreComputer: number;
  public scorePlayer: number;

  constructor(private gestureService: GestureService) {
    this.computer = Gesture.None;
    this.player = Gesture.None;
    this.result = Result.None;

    this.scoreComputer = 0;
    this.scorePlayer = 0;
  }

  startGame(gesture: Gesture) {
    this.player = gesture;
    this.computer = this.gestureService.getRadomGesture();
    this.result = this.gestureService.getResult(this.player, this.computer);

    switch (this.result) {
      case Result.Winner:
        this.scorePlayer++;
        break;
      case Result.Loser:
        this.scoreComputer++;
        break;
    }
  }
}
