import { Injectable } from '@angular/core';
import { Gesture } from '../model/gesture.enum';
import { Result } from '../model/result.enum';

@Injectable()
export class GestureService {
  private gestures: Gesture[];

  constructor() {
    this.gestures = [Gesture.Paper, Gesture.Rock, Gesture.Scissors];
  }

  getRadomGesture(): Gesture {
    const index = Math.floor(Math.random() * 2);
    return this.getGesture(index);
  }

  private getGesture(index: number): Gesture {
    switch (index) {
      case Gesture.Rock.valueOf():
        return Gesture.Rock;
      case Gesture.Paper.valueOf():
        return Gesture.Paper;
      case Gesture.Scissors.valueOf():
        return Gesture.Scissors;
      default:
        return Gesture.None;
    }
  }

  getResult(human: Gesture, computer: Gesture): Result {
    if (human === computer) {
      return  Result.Drawn;
    } else if (human === Gesture.Rock && computer === Gesture.Scissors) {
      return Result.Winner;
    } else if (human === Gesture.Paper && computer === Gesture.Rock) {
      return Result.Winner;
    } else if (human === Gesture.Scissors && computer === Gesture.Paper) {
      return Result.Winner;
    } else {
      return Result.Loser;
    }
  }
}
