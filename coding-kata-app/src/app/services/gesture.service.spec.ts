import { TestBed, async, inject } from '@angular/core/testing';

import { GestureService } from './gesture.service';
import { Gesture } from '../model/gesture.enum';
import { Result } from '../model/result.enum';

describe('GestureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GestureService]
    });
  });

  it('should be created', inject([GestureService], (service: GestureService) => {
    expect(service).toBeTruthy();
  }));

  it('random gesture should be not null', inject([GestureService], (service: GestureService) => {
    expect(service.getRadomGesture()).not.toBeNull();
  }));

  it('result of rock vs scissors should be win', inject([GestureService], (service: GestureService) => {
    expect(service.getResult(Gesture.Rock, Gesture.Scissors)).toEqual(Result.Winner)
  }));

  it('result of paper vs paper should be drawn', inject([GestureService], (service: GestureService) => {
    expect(service.getResult(Gesture.Paper, Gesture.Paper)).toEqual(Result.Drawn)
  }));

  it('result of paper vs scissors should be lose', inject([GestureService], (service: GestureService) => {
    expect(service.getResult(Gesture.Paper, Gesture.Scissors)).toEqual(Result.Loser)
  }));
});
