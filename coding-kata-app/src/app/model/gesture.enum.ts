export enum Gesture {
    None = -1,
    Rock = 0,
    Paper = 1,
    Scissors = 2
}
