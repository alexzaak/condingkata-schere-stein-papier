export enum Result {
    None = -1,
    Drawn = 0,
    Winner = 1,
    Loser = 2
}
