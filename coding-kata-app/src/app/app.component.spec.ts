import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { GestureService } from './services/gesture.service';
import { Gesture } from './model/gesture.enum';
import { Result } from './model/result.enum';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [GestureService],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`gesture of computer should be 'none'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.computer).toEqual(Gesture.None);
  }));

  it(`gesture of player should be 'none'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.player).toEqual(Gesture.None);
  }));

  it(`result should be 'none'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.result).toEqual(Result.None);
  }));

  it(`score of computer should be '0'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.scoreComputer).toEqual(0);
  }));

  it(`score of player should be '0'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.scorePlayer).toEqual(0);
  }));

  it(`gesture types should be a set of gestures`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.gestureTypes).toEqual(Gesture);
  }));

  it(`results types should be a set of results`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.resultTypes).toEqual(Result);
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Conding Kata - Schere Stein Papier');
  }));
});
